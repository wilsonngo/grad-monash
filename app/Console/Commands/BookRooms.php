<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\User;


class BookRooms extends Command {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'book:rooms';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Book Whereby rooms ahead of time';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle() {
    $users = User::where('always_live', 0)->get();
    $user_count = $users->count();
    $users->each(function ($user, $i) use ($user_count) {
      if ($user->bookRoom()) {
        echo ($i+1)." / ".$user_count."\n";
      } else {
        $user_count -= 1;
      }
    });
  }
}
