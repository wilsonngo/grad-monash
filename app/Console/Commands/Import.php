<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


class CSVFile {
  protected $name;
  protected $row;
  protected $rows = [];
  protected $columns = [];

  public function __construct($name) {
    $this->name = $name;
  }
  function open() {
    if (!file_exists($this->name)) {
      return $this->name.' could not be found';
    }

    $this->handle = fopen($this->name, 'r');
    $this->row = fgetcsv($this->handle);
    if (feof($this->handle)) {
      return 'No data was found in the file';
    }
    return true;
  }
  function getRow() {
    return $this->row;
  }
  function addColumn($k, $index) {
    $this->columns[$k] = $index;
  }
  function readUntilData() {
    if ($this->row) {
      $this->rows[] = $this->row;
      if ($this->hasData()) {
        return true;
      }
    }
    while (!feof($this->handle)) {
      $row = fgetcsv($this->handle);
      if (!$row) {
        continue;
      }
      $this->rows[] = $row;
      $this->row = $row;
      if ($this->hasData()) {
        return true;
      }
    }
    fclose($this->handle);
    unset($this->rows);

    $error = 'No items were found in the file. There was data, but either there were too few columns, or ';
    if (array_key_exists('ceremonyNumber', $this->columns)) {
        $error .= 'ceremony numbers are expected to be numeric';
    } else {
        $error .= 'celebration times could not be understood';
    }
    return $error;
  }
  function loadNextRow() {
    if (!empty($this->rows)) {
      $this->row = array_shift($this->rows);
      return true;
    } else if (is_array($this->rows)) {
      while (!feof($this->handle)) {
        $row = fgetcsv($this->handle);
        if (!$row) {
          continue;
        }
        $this->row = $row;
        return true;
      }
      fclose($this->handle);
      unset($this->rows);
    }
  }

  function hasData() {
    if (count($this->row) <= max($this->columns)) {
      return false;
    }
    if (array_key_exists('ceremonyNumber', $this->columns)) {
      return is_numeric($this->get('ceremonyNumber'));
    } else {
      return $this->getTimeAsSeconds() !== null;
    }
  }
  function getTimeAsSeconds() {
    $time = $this->get('celebrationTime');
    if ($time === null) {
      return;
    }
    $time = str_replace(':', '.', $time);
    if (!is_numeric($time)) {
      return;
    }
    if (strpos($time, '.') !== false) {
      list($minutes, $seconds) = explode('.', $time);
    } else {
      $minutes = $time;
      $seconds = 0;
    }
    return intval($minutes)*60 + intval($seconds);
  }
  function get($k) {
    if (array_key_exists($k, $this->columns)) {
      return $this->row[$this->columns[$k]];
    }
  }
  function set($k, $v) {
    $this->row[$this->columns[$k]] = $v;
  }
}

abstract class Import extends Command {

  protected $explained_columns = false;

  protected function getInputCSV($description, $columnData) {
    $csv_name = $this->ask($description);
    if (!$csv_name) {
      $this->error('Required');
      return;
    }
    $csv = new CSVFile($csv_name);
    $result = $csv->open();
    if ($result !== true) {
      $this->error($result);
      return;
    }

    $header = [];
    if ($this->confirm('Does the file have a header row?')) {
      $header = $csv->getRow();
      if (empty($header) || empty(implode(',', $header))) {
        $this->error('The first line of the file is empty');
        return;
      }
      $this->info(implode(',', $header));
    }

    $columnNumbers = [];
    foreach ($columnData as $k=>$columnItem) {
      $columnDescription = $columnItem['description'];
      if (!$this->explained_columns) {
        $columnDescription .= ' (';
        if (!empty($header)) {
          $columnDescription .= 'a header from the above list, or a number ';
        }
        $columnDescription .= 'starting from 0)';
        $this->explained_columns = true;
      }
      $default = !empty($header) && !empty($columnItem['header']) && in_array($columnItem['header'], $header);
      if ($default) {
        $columnDescription .= ' ['.$columnItem['header'].']';
      }
      $question = 'Which column '.$columnDescription.'?';
      $optional = !empty($columnItem['optional']);
      if ($optional) {
        $question .= ' Enter a blank value if not available yet.';
      }

      $column = $this->ask($question);
      if ($column === null) {
        if ($default) {
          $column = $columnItem['header'];
        } else if ($optional) {
          continue;
        }
      }
      if (in_array($column, $header)) {
        $csv->addColumn($k, array_search($column, $header));
      } else if (is_numeric($column)) {
        $csv->addColumn($k, intval($column));
      } else {
        $this->error('Invalid selection');
        return;
      }
    }

    $result = $csv->readUntilData();
    if ($result !== true) {
      $this->error($result);
    } else {
      return $csv;
    }
  }
}
