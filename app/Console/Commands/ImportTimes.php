<?php

namespace App\Console\Commands;

use Carbon\Carbon;

use App\Models\User;


class ImportTimes extends Import {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'import:times {dry-run?}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Imports celebration times from Monash Graduation spreadsheets';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle() {
    $dry_run = $this->argument('dry-run') === 'dry-run';
    if ($dry_run) {
      $this->info('Dry run only');
    }

    $grads_csv = $this->getInputCSV('Path to the graduates CSV, containing the celebration time and the previously created host links?', [
      'celebrationTime'=>[
        'description'=>'has the celebration time'
      ],
      'hostURL'=>[
        'description'=>'has the host link',
        'header'=>'Grad Party Host URL'
      ]
    ]);
    if (!$grads_csv) {
      return;
    }

    while ($grads_csv->loadNextRow()) {
      if ($grads_csv->hasData()) {
        $host_code = $grads_csv->get('hostURL');
        if (strpos($host_code, '.edu/') !== false) {
            $host_code = explode('.edu/', $host_code)[1];
        }

        $celebration_time = $grads_csv->getTimeAsSeconds();

        $user = User::where('host_code', $host_code)->first();
        if (!$user) {
          $this->error('User not found for host link: '.$host_code);
          continue;
        }
        $this->info($user->name.': Celebration: '.strval(floor($celebration_time / 60)).':'.strval($celebration_time % 60));
        if (!$dry_run) {
          $user->celebration_time = $celebration_time;
          $user->save();
        }
      }
    }
    $this->info('Finished');
  }
}
