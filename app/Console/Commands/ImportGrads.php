<?php

namespace App\Console\Commands;

use Carbon\Carbon;

use App\Models\User;
use App\Models\Video;


class ImportGrads extends Import {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'import:grads {dry-run?}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Imports users from Monash Graduation spreadsheets';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle() {
    $dry_run = $this->argument('dry-run') === 'dry-run';
    if ($dry_run) {
      $this->info('Dry run only');
    }

    $ceremony_csv = $this->getInputCSV('Path to the ceremonies CSV, containing the ceremony number and the date and time of the ceremony?', [
      'ceremonyNumber'=>[
        'description'=>'has the ceremony number',
        'header'=>'CEREMONY_NUMBER'
      ],
      'startsAt'=>[
        'description'=>'has the date and time',
        'header'=>'CEREMONY_DATE'
      ]
    ]);
    if (!$ceremony_csv) {
      return;
    }

    $grads_csv = $this->getInputCSV('Path to the graduates CSV (may be the same as the ceremonies CSV), containing the student names, ceremony number and celebration time?', [
      'fullName'=>[
        'description'=>'has the full name',
        'header'=>'GRADUATION_NAME'
      ],
      'firstName'=>[
        'description'=>'has the first name',
        'header'=>'GIVEN_NAMES'
      ],
      'lastName'=>[
        'description'=>'has the last name',
        'header'=>'SURNAME'
      ],
      'ceremonyNumber'=>[
        'description'=>'has the ceremony number',
        'header'=>'CEREMONY_NUMBER'
      ],
      'celebrationTime'=>[
        'description'=>'has the celebration time',
        'optional'=>true
      ],
      'hostCode'=>[
        'description'=>'should the host code be written to'
      ],
      'guestCode'=>[
        'description'=>'should the guest code be written to'
      ]
    ]);
    if (!$grads_csv) {
      return;
    }

    $ceremonies = [];
    while ($ceremony_csv->loadNextRow()) {
      if (!$ceremony_csv->hasData()) {
        continue;
      }
      $ceremonyNumber = $ceremony_csv->get('ceremonyNumber');
      if (!empty($ceremonies[$ceremonyNumber])) {
        // If there is only one input CSV, then the ceremony will be repeated for every student involved
        continue;
      }

      $starts_at = Carbon::parse(explode('-', $ceremony_csv->get('startsAt'))[0]);
      if ($starts_at->isMidnight()) {
        // No time has been provided, so set it to 11am
        $starts_at->hour = 11;
      }
      if ($dry_run) {
        $ceremonies[$ceremonyNumber] = $starts_at;
      } else {
        $video = new Video();
        $video->vimeo_url = 'https://vimeo.com/472045151/e75955cd39';
        $video->tencent_url = 'https://1257870645.vod2.myqcloud.com/da6afa0fvodcq1257870645/d33eacfe5285890809589714674/f0.mp4';
        $video->room_hours_duration = 13;

        $video->starts_at = $starts_at;
        $video->save();

        $ceremonies[$ceremonyNumber] = $video->id;
      }

      // 'Ceremony' users
      if ($dry_run) {
        $this->info('Ceremony '.$ceremonyNumber.': '.$starts_at->toDayDateTimeString());
      } else {
        $user = new User();
        $user->name = 'Ceremony '.$ceremonyNumber;
        $user->video_id = $video->id;
        $user->always_live = true;
        $user->saveWithHostCode();
        $this->info($user->name.' Host: https://graduation.monash.edu/'.$user->host_code.' Guest: https://graduation.monash.edu/'.$user->guest_code);
      }

      // 'Video' users
      if ($dry_run) {
        $this->info('Video '.$ceremonyNumber.': '.$starts_at->toDayDateTimeString());
      } else {
        $user = new User();
        $user->name = 'Video '.$ceremonyNumber;
        $user->video_id = $video->id;
        $user->save();
        $this->info($user->name.' Guest: https://graduation.monash.edu/'.$user->guest_code);
      }
    }

    if (!$dry_run) {
      $out_csv = $this->ask('What should the output CSV be called [out.csv]?') ?: 'out.csv';
      $out_handle = fopen($out_csv, 'w');
    }
    while ($grads_csv->loadNextRow()) {
      if ($grads_csv->hasData()) {
        // The input data provides the first and last names in uppercase,
        // but the full name in normal case
        // So this code attempts to extract the first name in normal case
        $fullName = $grads_csv->get('fullName');
        // Not everyone has a first name, at least according to the data
        $name = $grads_csv->get('firstName') ?: $grads_csv->get('lastName');
        // Extract the first (or last) name's amount of characters from the full name
        $string = substr($fullName, 0, strlen($name));
        if (strtoupper($string) !== substr($name, 0, strlen($string))) {
          // Making the extracted name uppercase doesn't actually match the uppercase first name
          // So just make the first letter of each word of first (or last) name uppercase
          $string = ucwords(strtolower($name));
        }

        $ceremony = $ceremonies[$grads_csv->get('ceremonyNumber')];

        $celebration_time = $grads_csv->getTimeAsSeconds();

        if ($dry_run) {
          $info = $string.'.';
          if ($celebration_time !== null) {
            $info .= ' Celebration: '.strval(floor($celebration_time / 60)).':'.strval($celebration_time % 60);
          }
          $info .= ' Ceremony: '.$ceremony->toDayDateTimeString();
          $this->info($info);
        } else {
          $user = new User();
          $user->name = $string;
          $user->video_id = $ceremony;
          $user->celebration_time = $celebration_time;
          $user->saveWithHostCode();

          $grads_csv->set('hostCode', 'https://graduation.monash.edu/'.$user->host_code);
          $grads_csv->set('guestCode', 'https://graduation.monash.edu/'.$user->guest_code);
        }
      }
      if (!$dry_run) {
        fputcsv($out_handle, $grads_csv->getRow());
      }
    }
    if (!$dry_run) {
      fclose($out_handle);
    }
    $this->info('Finished');
  }
}
