<?php

namespace App\Providers;

use App\WebSockets\IndexController;
use BeyondCode\LaravelWebSockets\Server\Router;
use Illuminate\Support\ServiceProvider;
use URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (config('app.env') === 'development') {
            URL::forceScheme('https');
        }

        $this->app->singleton('websockets.router', function () {
            $router = new Router();
            $router->get('/', IndexController::class);
            return $router;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
