<?php

namespace App\WebSockets;

use App\Models\User;
use BeyondCode\LaravelWebSockets\Apps\App;
use BeyondCode\LaravelWebSockets\Apps\ConfigAppProvider;

class GradConfigAppProvider extends ConfigAppProvider
{
    public function findByKey(string $appKey): ?App
    {
        /*
        Typically, the ConfigAppProvider would just authorise against the key,
        AuthServiceProvider authorise a user,
        and routes/channels.php would allow users into a channel.

        However, operating in that way means that the unauthorised users
        still have an open websocket. So instead, we are here authorising that
        the visitor is allowed into a channel at the same time, so that the websocket
        is closed if they are not.
        */
        $appAttributes = null;
        if (preg_match('/^(.*):code:([a-zA-Z0-9]+)$/', $appKey, $matches)) {
            if (User::getByCode($matches[2])) {
                $appAttributes = $this->apps
                                      ->firstWhere('key', $matches[1]);
            }
        }
        return $this->instanciate($appAttributes);
    }
}
