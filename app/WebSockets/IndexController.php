<?php
namespace App\WebSockets;

use BeyondCode\LaravelWebSockets\HttpApi\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psr\Http\Message\RequestInterface;
use Ratchet\ConnectionInterface;


class IndexController extends Controller {
    public function onOpen(ConnectionInterface $connection, RequestInterface $request = null) {
        $this->request = $request;

        $this->contentLength = $this->findContentLength($request->getHeaders());

        $this->requestBuffer = (string) $request->getBody();
        $connection->send(JsonResponse::create(['status'=>'running']));
        $connection->close();
	}

    public function ensureValidAppId(string $appId=null) {
        return $this;
    }

    public function __invoke(Request $request) {
    }
}
