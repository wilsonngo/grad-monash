<?php

namespace App\Http\Controllers;


class IndexController extends Controller
{
    public function __invoke() {
        $view = view('index', ['modal'=>'welcome']);
        return $this->responseWithHeaders($view);
    }
}
