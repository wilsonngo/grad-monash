<?php

namespace App\Http\Controllers;

use App;
use App\Models\Room;
use App\Models\User;
use Config;


class RoomController extends Controller
{
    public function __invoke($code=null, $name=null) {
        if (empty(Config::get('database.connections.mysql.password'))) {
            $view = view('room', ['modal'=>'late']);
            return $this->responseWithHeaders($view);
        }
        $user = User::getByCode($code);
        if (!$user) {
            abort(404);
        }

        $videoOnly = $user->videoOnly();
        if (!$videoOnly) {
            $user->bookRoom();
        }

        $room = $user->room;
        $host = $user->host_code == $code;
        $data = [
            'host'=>$host,
            'introModal'=>empty($name),
            'videoOnly'=>$videoOnly
        ];
        if (!$videoOnly) {
            $data['roomUrl'] = ($host ? $room->host_url.'&' : $room->url.'?').'embed&iframeSource=vmlaustralia&chat=on&people=off&background=off';
            $data['celebration_time'] = $user->celebration_time;
        }
        $event = $user->event();
        $data['event'] = $event;
        if ($host && empty($name)) {
            $name = $user->name;
        }
        $data['name'] = $name;
        if ($host) {
            $data['guest_code'] = $user->guest_code;
        }
        if (!$event->isActive() && !$user->alwaysLive()) {
            if (!$event->hasStarted()) {
                $starts_at = $event->starts_at;
                if (!$videoOnly) {
                    $starts_at = $starts_at->max($user->video->starts_at);
                }
                $data['starts_at'] = $starts_at;
                if (!$videoOnly) {
                    // Tell users it ends an hour before it does
                    $data['reported_ends_at'] = $event->ends_at->copy()->sub(1, 'hour')->format('ga');
                }
            } else {
                $data['modal'] = 'late';
            }
        }
        $user->log('Page loaded', $name);

        $view = view('room', $data);
        return $this->responseWithHeaders($view);
    }
}
