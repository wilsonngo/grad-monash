<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function responseWithHeaders($view) {
        return response($view)->header('X-Frame-Options', 'DENY')
                              ->header('X-Content-Type-Options', 'nosniff')
                              ->header('X-XSS-Protection', '1; mode=block')
                              ->header('Referrer-Policy', 'no-referrer')
                              ->header('Feature-Policy', 'autoplay *')
                              ->header('Strict-Transport-Security', 'max-age=63072000');
    }
}
