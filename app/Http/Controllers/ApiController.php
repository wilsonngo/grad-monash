<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Models\User;
use App\Events\SomeoneLeft;


class ApiController extends Controller
{
    protected function getVideoData($user) {
        $event = $user->event();
        if (!$event || !($event->hasStarted() || $user->alwaysLive())) {
            abort(403);
        }
        return [
            'video_url'=>request()->has('tencent') ? $user->video->tencent_url : $user->video->vimeo_url
        ];
    }
    public function country(Request $request) {
        $code = $request->input('code');
        $user = User::getByCode($code);
        if (!$user) {
            abort(404);
        }
        return $this->getVideoData($user);
    }
    public function name(Request $request) {
        $code = $request->input('code');
        $user = User::getByCode($code);
        if (!$user) {
            abort(404);
        }
        $inputName = $request->input('name');

        $name = $inputName;
        if (!$name) {
            $name = 'Guest';

            $guestNames = $user->logs()
                               ->where('event', 'Set name')
                               ->whereNull('data');
            if ($user->alwaysLive()) {
                $resetTime = now();
                $resetTime->hour = 3;
                $guestNames->where('created_at', '>', $resetTime);
            }
            $guest_name_count = $guestNames->count();
            if ($guest_name_count) {
                $name .= $guest_name_count;
            }
        }
        $user->log('Set name', $inputName);

        $data = $this->getVideoData($user);
        $data['name'] = $name;
        return $data;
    }
    public function someoneLeft(Request $request) {
        $code = $request->input('code');
        $user = User::where('host_code', $code)
                    ->first();
        if (!$user) {
            abort(403);
        }
        broadcast(new SomeoneLeft($user->guest_code));

        return [];
    }
}
