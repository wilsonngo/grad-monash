<?php

namespace App\Models;

class Video extends Event
{
    public $timestamps = false;

    protected $dates = ['starts_at'];

    public function users() {
        return $this->hasMany('App\Models\User');
    }

    public function hasStarted() {
        return now() >= $this->starts_at || request()->input('preview') === 'SNEAK_PEEK';
    }
    public function hasEnded() {
        return false;
    }

    public function roomsShouldEndAt() {
        return $this->starts_at->copy()->add($this->room_hours_duration, 'hour');
    }
}
