<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class Event extends Model {

    abstract public function hasStarted();
    abstract public function hasEnded();

    public function isActive() {
        return $this->hasStarted() && !$this->hasEnded();
    }

    public function surveyURL() {
        return env("MONASH_SURVEY_URL","");
    }

    public function videoEndActionEvent() {
        /*
            Semester 2, 2021: UMAC has vimeo videos setup differerntly from previous semester thus causing the videos not to stop when it has ended.
            This enables logic to pause the video when the "ended" event has been detected: ie .listener.on('ended', event => {...
            In the absent of this code logic - video will default to previous semester's behaviour (ie: controlled by vimeo).
        */
        return env('MONASH_VIDEO_ENDED_EVENT','');        
    }
}
