<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class User extends Model {

    public $timestamps = false;

    protected $dates = ['created_at'];

    public static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->addCode('guest');

            $model->created_at = $model->freshTimestamp();
        });
    }

    protected function addCode($type) {
        do {
            $code = Str::random(75);
        } while (validator([
            'host_code'=>$code,
            'guest_code'=>$code
        ], [
            'host_code'=>'unique:users',
            'guest_code'=>'unique:users'
        ])->fails());
        $this->{$type.'_code'} = $code;
    }

    public function saveWithHostCode() {
        $this->addCode('host');
        $this->save();
    }

    public function room() {
        return $this->hasOne('App\Models\Room');
    }

    public function video() {
        return $this->belongsTo('App\Models\Video');
    }

    public function logs() {
        return $this->hasMany('App\Models\Log');
    }

    public static function getByCode($code) {
        return self::where(function ($query) use ($code) {
                       $query->where('host_code', $code)
                             ->orWhere('guest_code', $code);
                   })->first();
    }

    public function videoOnly() {
        return empty($this->host_code);
    }
    public function event() {
        return $this->videoOnly() ? $this->video : $this->room;
    }
    public function alwaysLive() {
        if (env('MONASH_STAGING') && in_array($this->guest_code, ['tooearly', 'toolate'])) {
            return false;
        }
        return $this->always_live || env('MONASH_ALWAYS_LIVE');
    }
    public function bookRoom() {
        if (!$this->alwaysLive() && empty($this->video->starts_at)) {
            // This is not always live, but there isn't a video start time
            abort(403, 'Video does not have start time');
        }
        if ($this->room && ($this->room->ends_at >= ($this->alwaysLive() ? now() : $this->video->roomsShouldEndAt()))) {
            // Room already exists and is open long enough
            return false;
        }

        if ($this->room) {
            $room = $this->room;
        } else {
            $room = new Room();
            $room->user_id = $this->id;
        }

        // Even though the video may not have started yet,
        // starting the room now means that ?preview=SNEAK_PEEK can be used
        $start = now();

        if ($this->alwaysLive()) {
            $end = $start->copy()->add(1, 'day');
            $end->hour = 3;
        } else {
            $end = $this->video->roomsShouldEndAt();
            if ($end < $start) {
                // Room has already ended
                $room->ends_at = $end;
                $this->room = $room;
                return false;
            }
        }

        // https://whereby.dev/http-api/
        $response = Http::withHeaders([
            'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmFwcGVhci5pbiIsImF1ZCI6Imh0dHBzOi8vYXBpLmFwcGVhci5pbi92MSIsImV4cCI6OTAwNzE5OTI1NDc0MDk5MSwiaWF0IjoxNjAwODEwMDI1LCJvcmdhbml6YXRpb25JZCI6NDM4MzQsImp0aSI6IjQ3MzQ4ZWY5LTA5YzEtNGQ0MC04MmRjLTFkNzcyOTU4OGFmMiJ9.5qUd2-7Q8s5njPE6QAxad7K0ffPgZA5ZdMTNOBqLBew'
        ])->post('https://api.whereby.dev/v1/meetings', [
            'isLocked'=>true,
            'endDate' => $end->toJSON(),
            'roomNamePrefix'=>'/'.strtolower(Str::random(39)),
            'roomMode' => 'group',
            'fields'=>['hostRoomUrl']
        ]);

        $json = $response->json();
        if (empty($json['roomUrl'])) {
            $this->log('Whereby meeting create failed', json_encode($json));
            abort(500, 'Error booking room');
        }
        $room->url = $json['roomUrl'];
        $room->host_url = $json['hostRoomUrl'];
        if (env('MONASH_STAGING') && $this->guest_code == 'short') {
            $room->starts_at = now()->add(10, 'seconds');
            $room->ends_at = now()->add(20, 'seconds');
        } else {
            $room->starts_at = Carbon::parse($json['startDate'])->setTimezone($start->timezone);
            $room->ends_at = Carbon::parse($json['endDate'])->setTimezone($end->timezone);
        }
        $room->save();

        $this->log('Whereby meeting created', json_encode($json));
        $this->room = $room;
        return true;
    }

    public function log($event, $data=null) {
        $log = new Log();
        $log->user_id = $this->id;
        $log->ip_address = request()->ip();
        $log->event = $event;
        $log->data = $data;
        $log->save();
    }
}
