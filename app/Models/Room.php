<?php

namespace App\Models;

class Room extends Event {

    public $timestamps = false;

    protected $dates = ['starts_at', 'ends_at'];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function hasStarted() {
        if ($this->user->alwaysLive()) {
            return true;
        }
        return now() >= $this->starts_at && $this->user->video->hasStarted();
    }
    public function hasEnded() {
        if ($this->user->alwaysLive()) {
            return false;
        }
        return $this->ends_at < now();
    }
}
