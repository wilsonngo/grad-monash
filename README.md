# Monash Graduation Celebration

----
## Overview
Monash Graduation Celebration was created using Laravel 7. If you have not used Laravel before, you will find the SCSS under 'resources/sass' and the JS under 'resources/js'. jQuery is also used.

This site provides a separate virtual graduation space for each student. A student is also known as a 'host'. Within this graduation space, they may share the space with others, 'guests'. All participants can then communicate through an embedded iframe of the video conferencing service Whereby, and watch a video that is kept in sync. Each space is only open for a certain number of hours, and then it is closed. Hosts and guests are identified by whether they are using the unique 'host' link or the unique 'guest' link.

----
## Configuration
In the base of the project, you will find a '.env' file. This is used by Laravel to set
configuration variables. It is safe to put sensitive information in here because in
Laravel, the root of the web directory is /public.

The MySQL database connection details can be set through `DB_`.

`PUSHER_APP_`, `BROADCAST_DRIVER` and `LARAVEL_WEBSOCKETS_SSL_LOCAL_` are all used to configure websockets. You should only need to be concerned about `LARAVEL_WEBSOCKETS_SSL_LOCAL_` when moving to a new environment, as those are the paths to the SSL ceritifcate and key files.

`MONASH_ALWAYS_LIVE` is an option used to allow all rooms to always be live on staging. This is because using the staging site to preview a room is uninteresting if you are just shown a message saying that you are too early.

----
## Video
For most locations, the video shown is from Vimeo. This is controlled through the Vimeo Player SDK (https://developer.vimeo.com/player/sdk). Its methods and events are similar to HTML5 video methods and events.

Due China's firewall however, this is not an option for that country. Instead, videos are hosted in Tencent, and loaded as standard HTML5 video.

To detect whether a user is in China or not, the ping.js library is used, attempting to reach vimeo.com within 1 second. If it can, Vimeo is used. If not, Tencent.

Any participant in a graduation space can control the video, and all other participants in the space will see it reflected in their browser. To provide speed and performance, websockets were used (https://github.com/beyondcode/laravel-websockets).

Laravel WebSockets requires a separate port to run. It was found with the Monash team that this was complex to setup when moving to their environment.

Instead of opening and closing connections with traditional AJAX, websockets keep a connection open with the server.

The strategy settled on for keeping videos in sync was that when a user presses play, the video does not actually immediately play. Instead, it communicates to all other browsers in the space that a play event is scheduled, and then after a few seconds, all browsers play at once. To disguise this unexpected behaviour from users, the standard Vimeo and HTML5 controls were hidden, and custom controls implemented instead.

This also had the benefit of separating events triggered by the user from events triggered by the video. Using Javascript to control the video previously also triggered the same events as user actions. With custom controls however, events from the custom controls were clearly from the user.

Unfortunately, while the package chosen only supports sending messages from the server and broadcasting events to others in the same space, it does not provide support for using the connection to send data back to Laravel in an actionable way. So AJAX is still used for sensitive interactions.

One of these sensitive interactions is actually determining the URL of the video. The client does not wish for video URLs to be known until a space is open. So an AJAX request (/api/name if also reporting the name, or /api/country otherwise) is sent so that if the user has loaded the page early, the video details can be retrieved, and only when the server confirms that it is the right time.

----
## Video conferencing

The Whereby service does not provide Javascript control. There is an HTTP API (https://whereby.dev/http-api/) for creating, getting and deleting meetings, and some GET arguments can be passed to the iframe (https://whereby.dev/), but that is it.

One of the GET arguments is the user's name. Whereby has a 'Set display name' UI option though, and that will override this setting. This is argued to not a problem though, because people do not have a real need to change their name. In the Monash Graduation Celebration, hosts are automatically given the student's name from the database. If a guest does not supply a name, then they are 'Guest', and then 'Guest1', 'Guest2', etc.

When creating a Whereby room, we have chosen to create 'locked' rooms. Whereby provides two URLs, a URL for the host and a URL for guests, and guests have to click a 'Knock' button and then be accepted by the host to enter the room.

It was found that, undocumented, Whereby does post a 'participantupdate' Javascript message when the number of participants in a room changes. This was used to detect when a guest has entered a room (the 'participiantupdate' message is emitted for the first time) and when a Whereby participant was evicted (if everyone else sees a 'participantupdate' decrease but not you, then you were the one evicted).

----
## Importing

To import students and ceremonies, you can run 'php artisan import:grads' on the command line to import CSV data of the students and the ceremony dates, and output a CSV updated with host and guest links.

The difficulty with running this command for each graduation period is that Monash does not supply the data in exactly the same format each time. For the first graduation,
the students and the ceremony dates were in separate CSVs (with the Ceremony Number acting as the foreign key), and for the second, they were not. The script attempts to handle this variation, by asking which column and files the data is present in.

If you would like to check that the script is processing the input correctly, you can run 'php artisan import:grads dry-run' first, which does not modify the database.

This command also creates 'Ceremony' users, that have the 'always_live' MySQL column set to true, and 'Video' users, that only include the video and not Whereby, as indicated by the lack of a host code.

At the moment, the input data does not have Vimeo or Tencent URLs, so default values are used instead, and you will have to manually update the 'videos' table once the URLs are available. You will find a query like `UPDATE videos SET vimeo_url = "https://vimeo.com/528158003" WHERE id = (SELECT video_id FROM users WHERE created_at LIKE '2021%' AND name = "Ceremony 16");` helpful.

If the spreadsheet does not specify a start time for the ceremonies, but only a start date, then a hardcoded default of 11am is used. The duration of the rooms at the time of import is hardcoded at 13 hours.

The initial data to be imported may not have the celebration time. If that is case, once the data does become available, it be imported through 'php artisan import:times', using a CSV that listing the host URLs created earlier for each student and their celebration times. It also hs a dry run option, 'php artisan import:times dry-run'.

----
## Booking rooms

While the Whereby rooms are created automatically as users arrive, there is also the option of creating rooms ahead of time, making the calls to the Whereby API in advance. Running 'php artisan book:rooms' on the command line performs this task.

If you would like to extend the duration of a room for a set of videos after they have already been booked, simply update 'room_hours_duration' in the 'videos' table. Then, when a user arrives or the command line task is run, the room will be rebooked.

As such, no action needs to be taken at the start of a graduation week. With all of the information in the database, when users arrive at the right time, they will simply no longer see a message saying they are too early, and enter the room instead.

----
## Conditions

In the code, because of the various conditions that need to be met before certain events are triggered, a custom utility was created. Conditions can be marked as 'ready' or 'unready', and 'when' one or more conditions are met, then a promise is resolved.

These conditions are
- is the user in the Whereby room yet? ('inside')
- has the JS determined whether they are in China yet? ('country')
- has the video URL been retrieved from the server yet? ('video_url')
- is the user online? ('online')
- has the user granted permission for the camera and microphone to be used? ('permissions')
- is this the right time for the space to be open? ('active')
- has the user connects to websockets? ('subscribed')
- is the host online? ('host')

----
## Previewing

If Monash staff would like to enter a room early, '?preview=SNEAK_PEEK' can be appended to a URL. If Monash staff would like to check how the room would appear if the user is in China, then '?preview=SNEAK_PEEK&country=CHINA' can be used.

----

# Local Setup for Dev : Steps on MacBook Pro (2021 Nov)

  

**References**:

-   Andrew Murray email notes. https://confluence.apps.monash.edu/x/PYVSD.
-   Laravel 7 docs: [https://laravel.com/docs/7.x/installation](https://laravel.com/docs/7.x/installation#server-requirements)

**Note**

Whereby and Websocket: have not attempted as front-end changes were required.

  

**Let's do this - Godspeed!**

1.  Install MAMP stack ( at the very least mySQL and PHP)
    > However, if you are not using Homestead, you will need to make sure your server meets the following requirements:
        > 
        > -   PHP >= 7.2.5
        > -   BCMath PHP Extension
        > -   Ctype PHP Extension
        > -   Fileinfo PHP extension
        > -   JSON PHP Extension
        > -   Mbstring PHP Extension
        > -   OpenSSL PHP Extension
        > -   PDO PHP Extension
        > -   Tokenizer PHP Extension
        > -   XML PHP ExtensionEnsure all the PHP modules are present ([https://laravel.com/docs/7.x/installation#server-requirements](https://laravel.com/docs/7.x/installation#server-requirements))
        
2.  Install composer:  [https://getcomposer.org/download/](https://getcomposer.org/download/)
3.  In project root
    
    ```bash
    npm install
    npm run dev
    composer global require laravel/installer #download the installer (optional)
    composer install #install project dependencies
    ```
    
4.  In the  **.env**  file
    1.  Ensure  **DB_**  section is configured to point to your instance with correct credentials.
    2.  Add the following to update the rendered base url otherwise it will render https
        ```bash
            ASSET_URL=. #comment out in prod  [https://laravel.com/docs/7.x/helpers#method-asset](https://laravel.com/docs/7.x/helpers#method-asset)
        ```
5.  Database setup (skip if you already have an existing instance)
    1.  Create table in mysql matching the  **.env**  entry **DB_NAME**
    2.  Execute:
	    ```bash
	       php artisan migrate #create database structure
	       php artisan db:seed #create dummy data
		```
        
6.  Serving the app
    
    * Using the local PHP server
		```bash
	    npm run dev # build the front-end project
	    php artisan serve #local PHP server
	    ```
    * Alternatively, you can use Apache (point  the document root at the ‘public’ directory).


## References / Troubleshoot

* Permission to storage for caching
    ```bash
        chmod -R gu+w storage
        chmod -R guo+w storage
        php artisan cache:clear
    ```
* Mix not found.
  1. Update views to use `asset` keywords instead of `mix`.
  2. Ensure .env file contains when using `asset` keyword.
  ```bash
      ASSET_URL=.
  ```
  3. Execute:
  ```bash
      npm install
      npm run dev
  ```