<?php

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Schema;

use App\Models\User;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

if (!empty(Config::get('database.connections.mysql.password')) && Schema::hasTable('users')) {
    User::all()->each(function ($roomUser) {
        Broadcast::channel($roomUser->guest_code, function ($user) use ($roomUser) {
            // Authorise if they are able to join this room
            return $user->id == $roomUser->id;
        }, ['guards'=>['monashguard']]);
    });
}
