<?php

use Illuminate\Support\Facades\Route;


Route::get('/country', 'ApiController@country');
Route::post('/name', 'ApiController@name');

Route::post('/someoneLeft', 'ApiController@someoneLeft');
