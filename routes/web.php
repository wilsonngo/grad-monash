<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'IndexController');

Route::get('/gown/{id?}', function ($id='') {
    // $id may be one of
    // '13a4','980a','ce50','8077','696e','06a6','38df','95e5','096e','b690','e384','49ed','0fa1','b0ff','8d61','015e','9ef0','f099','9577','41bc','6bbb','e4dc','609b','dbcc','1a30','1113','18cc','b2e3','2611','392b','8f23','8197','1a62','757b','b417'
    // or an empty string

    $url = 'https://vmlyr.8thwall.app/monashgrad';
    if ($id) {
        $url .= '/?code='.$id;
    }
    return redirect()->away($url);
})->where('id', '[a-z0-9]+');

Route::get('/{code}/{name?}', 'RoomController')
     ->where('room', '^(?!api)[a-zA-Z0-9]+');
