const $ = require('jquery')

import { ready } from './util/when'


let modals
let currentModal

let queued
let queuedItems = []
export function queue(item) {
    if (queued) {
        queuedItems.push(item)
    } else {
        setTimeout(item, 0)
        queued = true

        const nextItem = () => {
            item = queuedItems.shift()
            if (item !== undefined) {
                item()

                setTimeout(nextItem, 500)
            } else {
                queued = false
            }
        }
        setTimeout(nextItem, 500)
    }
}

function init() {
    modals = []

    const $modal = $('.modal.visible')
    if ($modal.length) {
        const modal = {
            id: $modal.attr('id')
        }
        modals.push(modal)
        currentModal = modal
    }
}

export function showModal(id, priority) {
    if (modals === undefined) {
        init()
    }
    // Do not allow a single modal to be open more than once
    modals = modals.filter(modal => modal.id !== id)

    return new Promise((resolve, reject) => {
        const modal = {id, priority, resolve}
        modals.push(modal)

        if (!currentModal || !(currentModal.priority && !priority)) {
            const action = () => {
                currentModal = modal

                const $modal = $('.modal#'+id)
                $modal.addClass('visible')
            }
            if (currentModal) {
                $('.modal').removeClass('visible')
                queue(action)
            } else {
                $('body').addClass('modalVisible')
                action()
            }
        }
    })
}

export function closeModal(id, onlyIfClosable) {
    if (modals === undefined) {
        init()
    }
    const $modal = $('.modal#'+id)
    if (onlyIfClosable && !$modal.find('> .inner > .close').length) {
        return
    }

    $modal.removeClass('visible')

    modals = modals.filter(modal => modal.id !== id)
    if (currentModal && id === currentModal.id) {
        const resolve = currentModal.resolve

        if (modals.length) {
            currentModal = modals.find(modal => modal.priority) || modals[modals.length-1]
            queue(() => {
                $('.modal#'+currentModal.id).addClass('visible')

                resolve()
            })
        } else {
            currentModal = undefined
            $('body').removeClass('modalVisible')

            resolve()
        }
    }
}
