const $ = require('jquery')

import { showModal, queue } from './modals'
import { when, ready } from './util/when'


class VideoControls {
    constructor(vi, callbacks) {
        this.playing = false
        this.callbacks = callbacks
        this.currentTime = 0
        vi.controls = this

        const ua = navigator.userAgent.toLowerCase()
        this.ios = ua.indexOf('iphone os ') !== -1 || ua.indexOf('cpu os ') !== -1

        const $item = $('<div />').addClass('controls widescreen')
        $('.videoContainer').append($item)

        this.$playPause = $('<div />').addClass('playPause paused')
        this.$progress = $('<progress />').attr('value', '0')
        this.$volume = $('<div />').addClass('volume')
        this.$volumeProgress = $('<progress />')
        this.$volume.append(this.$volumeProgress)
        

        /* larger video toggle */
        this.$expandVideo = $('<div title="Expand/Shrink video" />').addClass('videoExpandContainer').css('display','none')
        this.$expandVideoButton = $('<button />').addClass('videoExpand').append($('<span />').addClass('icon-expand'))
        this.$expandVideo.append(this.$expandVideoButton)

        /* text track */
        this.$textTrack = $('<div />').addClass('textTrackContainer').css('display','none')
        const $textTrackSelect = $('<div />').addClass("hidden wrapper")
        this.$textTrackButton = $('<button />').addClass('closed-captions toggle cc off')
        const svg = $.parseHTML('<svg viewBox="0 0 20 14" focusable="false" aria-labelledby="cc-icon-title" role="img"><title id="cc-icon-title">Choose captions</title><path class="fill" fill-rule="evenodd" clip-rule="evenodd" d="M17 0h-14c-1.657 0-3 1.343-3 3v8c0 1.656 1.343 3 3 3h14c1.657 0 3-1.344 3-3v-8c0-1.657-1.343-3-3-3zm-7.271 8.282c-.145.923-.516 1.686-1.105 2.268-.597.591-1.369.89-2.294.89-1.138 0-2.049-.402-2.706-1.195-.647-.786-.975-1.866-.975-3.215 0-1.458.372-2.603 1.105-3.403.65-.708 1.487-1.067 2.487-1.067 1.33 0 2.321.482 2.947 1.435.34.53.526 1.072.553 1.611l.013.236h-1.984l-.044-.169c-.092-.355-.207-.622-.343-.793-.239-.298-.591-.443-1.076-.443-.483 0-.856.209-1.14.641-.298.455-.449 1.12-.449 1.977 0 .851.156 1.49.466 1.898.298.395.666.588 1.122.588.469 0 .814-.16 1.058-.491.138-.183.255-.472.351-.856l.042-.17h2.013l-.041.258zm7.582 0c-.145.923-.516 1.686-1.104 2.268-.598.591-1.369.89-2.294.89-1.139 0-2.049-.402-2.707-1.195-.646-.785-.975-1.865-.975-3.214 0-1.458.372-2.603 1.106-3.403.649-.708 1.485-1.067 2.486-1.067 1.33 0 2.32.482 2.946 1.435.34.53.526 1.072.554 1.611l.012.236h-1.9829999999999999l-.043-.169c-.092-.355-.208-.623-.344-.793-.238-.298-.591-.443-1.076-.443-.483 0-.856.209-1.139.641-.299.455-.45 1.12-.45 1.977 0 .851.157 1.49.467 1.898.299.395.666.588 1.121.588.469 0 .814-.16 1.058-.491.138-.183.256-.472.352-.856l.042-.17h2.012l-.041.257z"></path></svg>')
        this.$textTrackButton.append(svg)
        this.$textTrack.append($textTrackSelect)
        this.$textTrack.append(this.$textTrackButton)

        /* time, loading, hover */
        this.$time = $('<div />').addClass('time')
        this.$loading = $('<div />').addClass('loading')
        this.$hoverTime = $('<div />').addClass('hoverTime')

        if (!vi.tencent) {
            this.$textTrack.css('display','block')
            $textTrackSelect.append($.parseHTML('<label class="ctn">None <input type="radio" checked="checked" name="radio" value="none"> <span class="checkmark"></span></label>'))
            // Get text track info
            const $textTrackButton = this.$textTrackButton
            vi.item.getTextTracks().then(function(tracks) {
                if (tracks.length) {
                    for (var track in tracks) {
                        track = tracks[track];
                        //$textTrackSelect.append('<option value="' + track.language + '.' + track.kind + '"' + (track.mode === 'showing' ? ' selected' : '') + '>' + track.label + '</option>');
                        $textTrackSelect.append(`<label class="ctn">${track.label} <input type="radio" value="${track.language + '.' + track.kind}" name="radio"> <span class="checkmark"></span></label>`);
                    }

                    $textTrackSelect.find('input[type=radio]').on('change', function() {
                        const textTrack = $(this).val();
                        $textTrackSelect.addClass("hidden")
                        if (textTrack === 'none') {
                            vi.disableTextTrack()
                            $textTrackButton.removeClass("on").addClass("off")
                            return
                        }else {
                            var id = textTrack.split('.');
                            vi.enableTextTrack(id[0], id[1])
                            $textTrackButton.removeClass("off").addClass("on")
                        }
                    })

                }
            }).catch((e) => console.error(e.message));
        }

        const durationMatched = duration => {
            this.duration = duration

            this.updateTime(this.currentTime)

            $item.append(this.$playPause)
            $item.append(this.$progress)
            $item.append(this.$hoverTime)

            let val
            let muted
            const setVolumeProgress = updatePlayer => {
                const shown = muted ? 0 : val
                this.$volumeProgress.val(shown)

                let level
                if (shown < 0.25) {
                    level = 'muted'
                } else if (shown >= 0.25 && shown < 0.5) {
                    level = 'third'
                } else if (shown >= 0.5 && shown < 0.75) {
                    level = 'half'
                }
                if (level) {
                    this.$volume.attr('data-level', level)
                } else {
                    this.$volume.removeAttr('data-level')
                }

                if (updatePlayer) {
                    vi.setVolume(shown)
                }
            }
            vi.getVolume().then(volume => {
                val = volume
                setVolumeProgress()

                $item.append(this.$volume)
                $item.append(this.$textTrack)
                $item.append(this.$expandVideo)
                $item.append(this.$time)
            })
            $('.videoContainer').append(this.$loading)

            this.$playPause.on('click', () => {
                if (this.loading) {
                    return
                }
                this.updatePlaying(this.$playPause.hasClass('paused'))

                if (this.ios === true && this.playing) {
                    vi.play()
                    this.ios = 'mid'
                } else {
                    if (callbacks['play'] === undefined && this.playing) {
                        vi.play()
                    } else {
                        vi.pause()
                    }
                }
                if (this.playing) {
                    if (callbacks['play'] !== undefined) {
                        callbacks['play']()
                        this.updateLoading(true)
                    }
                } else {
                    callbacks['pause']({
                        playing: false,
                        time: this.currentTime
                    })
                }
            })

            this.$textTrackButton.on('click', () =>{ 
                $textTrackSelect.toggleClass("hidden")
            })

            this.$expandVideo.on('click', () =>{ 
                this.$expandVideoButton.find('span.icon-expand').toggleClass('shrink')
                $('body').toggleClass('min')
                if (typeof(this.callbacks['expandVideoClick']) === 'function') {
                    this.callbacks['expandVideoClick']()
                }
            })

            const timeFromEvent = pos => Math.min(1, Math.max(0, event.offsetX / this.$progress.width())) * this.duration
            this.$progress.on('click', event => {
                if (this.loading) {
                    return
                }
                this.updatePlaying(false)

                vi.pause()

                const time = timeFromEvent(event)
                this.updateTime(time, true)

                this.ignoreTimeUpdate = true
                vi.setCurrentTime(time).finally(() => {
                    this.ignoreTimeUpdate = false
                })

                callbacks['seeked']({
                    playing: false,
                    time: this.currentTime
                })
            })
            this.$progress.on('mouseenter mousemove', event => {
                this.$hoverTime.addClass('visible')
                this.$hoverTime.css('left', event.offsetX)

                const time = timeFromEvent(event)
                this.$hoverTime.text(this._formatTime(time))
            })
            this.$progress.on('mouseleave', () => {
                this.$hoverTime.removeClass('visible')
            })

            this.$volumeProgress.on('click', event => {
                val = Math.min(1, Math.max(0, event.offsetX / this.$volumeProgress.width()))
                muted = false
                setVolumeProgress(true)
            })
            this.$volume.on('click', event => {
                if (event.target.className === 'volume') {
                    muted = !muted
                    setVolumeProgress(true)
                }
            })
        }
        const checkDuration = () => {
            vi.getDuration().then(duration => {
                if (duration === 0) {
                    setTimeout(checkDuration, 2000)
                } else {
                    durationMatched(duration)
                }
            })
        }
        checkDuration()
    }
    _formatTime(seconds) {
        let timeString
        const appendPart = (amount, pad) => {
            let amountString = (amount % 60).toString()
            if (pad) {
                amountString = amountString.padStart(2, '0')
            }
            timeString = timeString ? amountString+':'+timeString : amountString
            return Math.floor(amount / 60)
        }
        const minutes = appendPart(Math.floor(seconds), true)
        const hours = appendPart(minutes, minutes >= 60)
        if (hours) {
            appendPart(hours)
        }
        return timeString
    }
    getState() {
        return {
            playing: this.playing,
            time: this.currentTime
        }
    }
    updateTime(seconds, doNotIgnore) {
        if (this.ignoreTimeUpdate && !doNotIgnore) {
            return
        }
        this.currentTime = seconds
        if (this.duration === undefined) {
            return
        }
        this.$progress.val(seconds / this.duration)

        this.$time.text(this._formatTime(seconds))
    }
    updatePlaying(playing) {
        this.playing = playing
        this.$playPause.toggleClass('paused', !this.playing)
    }

    _updateLoading() {
        this.$loading.toggleClass('visible', this.loading || this.buffering)
    }
    updateLoading(loading) {
        this.loading = loading
        this._updateLoading()
    }
    updateBuffering(buffering) {
        this.buffering = buffering
        this._updateLoading()
    }
}

class VideoInterface {
    _showControls(duration) {
        if (this.startingZero) {
            $('.videoContainer').addClass('controlsVisible')
        } else {
            clearTimeout(this.controlsTimeout)

            $('.videoContainer').addClass('controlsVisible')
            this.controlsTimeout = setTimeout(() => {
                $('.videoContainer').removeClass('controlsVisible')
            }, duration || 3000)
        }
    }

    play() {
        if (this.interactionRequired) {
            return
        }
        this._showControls()

        const interactionRequired = () => {
            this.interactionRequired = true

            this.callbacks['interactionRequired']().then(() => {
                if (this.controls.ios === 'mid') {
                    this.item.play()
                    this.controls.ios = false
                }
                this.interactionRequired = false
            })
        }
        if (this.controls.ios === true) {
            this.controls.ios = 'mid'
            interactionRequired()
        } else {
            this.item.play().catch(e => {
                if (e.message.indexOf('play() failed because the user didn\'t interact with the document first') === 0) {
                    interactionRequired()
                }
            })
        }
    }
    pause() {
        this._showControls()
        this.item.pause()
    }
    setCurrentTime(time) {
        this._showControls()
        if (this.tencent) {
            this.item.currentTime = time

            return Promise.resolve()
        } else {
            return this.item.setCurrentTime(time)
        }
    }
    getDuration() {
        if (this.tencent) {
            return new Promise((resolve, reject) => {
                this.listener.on('loadedmetadata', () => {
                    resolve(this.item.duration)
                })

                const duration = this.item.duration
                if (duration) {
                    resolve(duration)
                }
            })
        } else {
            return this.item.getDuration()
        }
    }
    getVolume() {
        return this.tencent ? Promise.resolve(this.item.volume) : this.item.getVolume()
    }
    setVolume(volume) {
        if (this.tencent) {
            this.item.volume = volume
        } else {
            this.item.setVolume(volume)
        }
    }
    addCallbacks(callbacks) {
        this.callbacks = callbacks

        this.startingZero = true
        this._showControls()

        this.listener.on('pause', () => {
            if (this.controls.playing && !this.interactionRequired) {
                if (this.controls.currentTime < this.controls.duration) {
                    const recover = () => {
                        clearTimeout(this.recoverTimeout)
                        if (!this.controls.ios) {
                            this.play()
                        }
                    }
                    const now = new Date().getTime()
                    if (this.lastRecoveredFromPause !== undefined || now - this.lastRecoveredFromPause > 1000) {
                        recover()
                    } else {
                        this.recoverTimeout = setTimeout(recover, 1000)
                    }
                } else {
                    this.controls.updatePlaying(false)
                }
            }

            // no need to show near end of video
            //if (( this.controls.currentTime / this.controls.duration) >= 0.93) {
            //    callbacks['showSurvey'](null)
            //}

        })
        this.listener.on('seeked', () => {
            if (!this.controls.playing) {
                this.pause()
            }
        })
        this.listener.on('timeupdate', event => {
            const seconds = this.tencent ? this.item.currentTime : event.seconds
            this.controls.updateTime(seconds)
            callbacks['timeupdate'](seconds)

            if (seconds > 0 && this.startingZero) {
                this.startingZero = false
                this._showControls()

                const $clickDetect = $('<div />').addClass('clickDetect')
                $clickDetect.insertBefore($('.videoContainer .controls'))
                $clickDetect.click(() => {
                    this._showControls(7500)
                    $(".survey-cta-wrapper.middle").removeClass("middle")
                })
            }
        })
        if (this.tencent) {
            this.listener.on('waiting', () => {
                this.controls.updateBuffering(true)
            })
            this.listener.on('canplaythrough', () => {
                this.controls.updateBuffering(false)
            })
        } else {
            this.listener.on('bufferstart', () => {
                this.controls.updateBuffering(true)
            })
            this.listener.on('bufferend', () => {
                this.controls.updateBuffering(false)
            })
        }

        this.listener.on('ended', event => {
            callbacks['overlay'](event)
            //callbacks['showSurvey'](event)
        })

    }
    constructor(tencent, id) {
        this.tencent = tencent

        const $container = $('.videoContainer')
        if (tencent) {
            const $video = $('<video />')
            $video.attr('id', 'video')
            $video.attr('preload', 'auto')

            const $source = $('<source />')
            $source.attr('src', id)
            $source.attr('type', 'video/mp4')
            $video.append($source)

            $container.prepend($video)

            this.listener = $video
            this.item = $video[0]
        } else {
            const $iframe = $('<iframe />')
            $iframe.attr('id', 'video')
            $iframe.attr('src', 'https://player.vimeo.com/video/'+id+'?autopause=0&controls=0&title=0&byline=0')
            $iframe.attr('frameborder', '0')
            $iframe.attr('allow', 'autoplay')
            $container.prepend($iframe)

            this.listener = new Vimeo.Player($iframe[0])
            this.item = this.listener
        }
    }
    enableTextTrack(locale, trackName) {
        this.item.enableTextTrack(locale, trackName)
    }
    disableTextTrack() {
        this.item.disableTextTrack()
    }
}

export class Video {
    addAnimation(times, layer, data) {
        if (!this.animations) {
            this.animations = []
        }
        if (typeof times !== 'object') {
            times = [times]
        }
        const animation = {
            start: times[0],
            end: times[1] !== undefined ? times[0]+times[1] : undefined,
            layer,
            data
        }
        this.animations.push(animation)

        if (!$('object[data-name="'+animation.data+'"]').length) {
            const $object = $('<object />')
            $object.attr('type', 'image/svg+xml')
            $object.attr('data-layer', animation.layer)
            $object.attr('data-name', animation.data)
            $object.attr('data', '/images/animations/'+animation.data)
            $('.background').append($object)
            if (animation.start !== -1) {
                $object.addClass('hidden')
            }
        }
    }
    onTimeUpdate(seconds) {
        const $background = $('.background')
        const layers = {}
        this.animations.forEach(animation => {
            if (seconds >= animation.start && (animation.end === undefined || seconds < animation.end)) {
                layers[animation.layer] = animation.data
            }
        })

        const paths = Object.values(layers)
        const video = this
        $background.find('object, .gif').each(function () {
            const name = $(this).attr('data-name')
            const visible = paths.includes(name)
            $(this).toggleClass('hidden', !visible)

            if ($(this).hasClass('gif')) {
                if (visible && !video.gifInProgress) {
                    video.gifInProgress = true
                    video.gifs[0].click()
                    setTimeout(() => {
                        video.gifInProgress = false
                    }, 5130)
                }
            } else {
                const $svg = $(this).contents().find('svg')
                $svg.toggleClass('monash', visible)
            }
        })
    }
    getState() {
        return this.controls.getState()
    }
    onVideoEnd(callback) {
        this.callbackVideoEnd = callback
    }
    onUpdate(callback) {
        // Used by app.js to set the callback function for 'reportUserAction'
        this.callback = callback
    }
    onExpandVideoClick(callback) {
        this.callbackExpandVideoClick = callback
    }
    reportUserAction(data) {
        // Broadcast the status
        if (!this.callback || $('#interaction').hasClass('visible')) {
            return
        }

        this.callback(data)
    }
    update(data) {
        if (this.vi === undefined) {
            // If not ready yet, store the data for later
            if (this.dataWhenReady === undefined) {
                this.dataWhenReady = data
            } else {
                this.dataWhenReady = Object.assign(this.dataWhenReady, data)
            }
            return
        }
        if (this.dataWhenReady !== undefined) {
            data = Object.assign(this.dataWhenReady, data)

            this.dataWhenReady = undefined
        }

        const time = data.time
        if (time !== undefined) {
            this.vi.setCurrentTime(time)
            this.controls.updateTime(time)
        }

        if (data.playing === true) {
            this.vi.play()
            this.controls.updatePlaying(true)
        } else if (data.playing === false) {
            this.vi.pause()
            this.controls.updatePlaying(false)
        }
    }
    fetchCountry() {
        const MonashCode = window.location.pathname.split('/')[1]
        when(['online', 'country']).then(() => {
            let url = '/api/country?code='+MonashCode
            if (this.china) {
                url += '&tencent=true'
            }
            if (window.location.search.indexOf('preview=SNEAK_PEEK') !== -1) {
                url += '&preview=SNEAK_PEEK'
            }
            axios.get(url).then(response => {
                if (response.data.video_url !== undefined) {
                    this.url = response.data.video_url
                    ready('video_url')
                }
            })
        })
    }
    setup() {
        this.vi = new VideoInterface(this.china, this.china ? this.url : this.url.split('/')[3])
        this.controls = new VideoControls(this.vi, this.room ? {
            'play': event => {
                this.room.countdownToSync()
            },
            'pause': event => {
                this.reportUserAction(event)
            },
            'seeked': event => {
                event.event = 'seeked'
                this.reportUserAction(event)
                this.onTimeUpdate(event.time)
            },
            'expandVideoClick': () => { if (this.callbackExpandVideoClick) this.callbackExpandVideoClick() }
            
        } : {
            'pause': () => {},
            'seeked': event => {
                this.onTimeUpdate(event.time)
            },
            'expandVideoClick': () => { if (this.callbackExpandVideoClick) this.callbackExpandVideoClick() }
        })
        this.vi.addCallbacks({
            'timeupdate': seconds => {
                this.onTimeUpdate(seconds)
            },
            'interactionRequired': event => {
                return this.room ? new Promise((resolve, reject) => {
                    queue(() => {
                        showModal('interaction').then(() => {
                            resolve()
                            this.room.requestUpdate('interaction').catch(() => {
                                this.vi.play()
                            })
                        })
                    })
                }) : Promise.resolve()
            },
            'showSurvey': event => {
                
                $(".survey-cta-wrapper.middle").removeClass("middle")
                queue(() => {
                    showModal('survey', true)
                })
                
            },
            'overlay': event => {
                if (this.callbackVideoEnd) {
                    this.callbackVideoEnd(true, event)
                }
            },
        })
    }
}
