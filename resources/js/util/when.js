let promises = []
let readyEvents = []

export function when(events) {
    if (typeof events === 'string') {
        events = [events]
    }
    return new Promise((resolve, reject) => {
        if (events.every(promiseEvent => readyEvents.includes(promiseEvent))) {
            // Already ready
            resolve()
        } else {
            promises.push({
                events,
                resolve
            })
        }
    })
}
export function ready(event) {
    readyEvents.push(event)

    promises.forEach(promise => {
        if (promise.events.every(promiseEvent => readyEvents.includes(promiseEvent))) {
            promise.resolve()

            promises = promises.filter(filterPromise => filterPromise !== promise)
        }
    })
}
export function unready(event) {
    readyEvents = readyEvents.filter(readyEvent => readyEvent !== event)
}
export function isReady(event) {
    return readyEvents.includes(event)
}
