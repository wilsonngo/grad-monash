const $ = require('jquery')

import Echo from 'laravel-echo'

import { showModal, queue } from './modals'
import { when, ready, unready } from './util/when'

const requestTimeout = 2500
const countdownSyncDuration = 2000


export class Room {
    setName(name) {
        return new Promise((resolve, reject) => {
            when(['online', 'country']).then(() => {
                const data = {
                    name,
                    code: this.MonashCode
                }
                if (this.video.china) {
                    data.tencent = true
                }
                if (window.location.search.indexOf('preview=SNEAK_PEEK') !== -1) {
                    data.preview = 'SNEAK_PEEK'
                }
                axios.post('/api/name', data).then(response => {
                    if (response.data.video_url !== undefined) {
                        this.video.url = response.data.video_url
                        ready('video_url')
                    }

                    resolve(response.data.name)
                }).catch(() => {
                    resolve('Guest')
                })
            })
        })
    }
    respondToUpdateRequestReply(data) {
        this.updateRequestData = undefined
        this.video.update({
            time: data.time,
            playing: data.playing
        })
    }
    requestUpdate(source) {
        // 'source' is one of 'load' or 'interaction'
        return new Promise((resolve, reject) => {
            if (this.outside) {
                return
            }
            this.updateRequestData = {
                time: new Date().getTime(),
                source
            }
            this.e.whisper('request')

            if (source !== 'load') {
                // If no one else is in the room, give up after 2.5s
                this.updateTimeout = setTimeout(reject, requestTimeout)
            }
        })
    }
    preventInteraction(timeout) {
        if (timeout !== undefined) {
            setTimeout(() => {
                if (this.video.controls.ios === 'mid') {
                    this.video.vi.setCurrentTime(this.video.controls.currentTime)
                    this.video.controls.ios = false
                }
                this.video.vi.play()
                this.video.controls.updatePlaying(true)
                this.video.controls.updateLoading(false)
                this.countdownInProgress = undefined
            }, timeout)
        } else {
            this.video.controls.updateLoading(true)
        }
    }
    countdownToSync() {
        if (this.countdownInProgress) {
            return
        }
        this.countdownInProgress = {startedByMe: true}
        this.e.whisper('countdownStart')
        this.preventInteraction(countdownSyncDuration)
    }
    constructor(host, video) {
        /**
         * Echo exposes an expressive API for subscribing to channels and listening
         * for events that are broadcast by Laravel. Echo and event broadcasting
         * allows your team to easily build robust real-time web applications.
         */
        if (process.env.MIX_PUSHER_APP_KEY === undefined) {
            return
        }
        this.video = video

        window.Pusher = require('pusher-js')

        let hostCode
        if (host) {
            hostCode = window.location.pathname.split('/')[1]
            this.MonashCode = $('.wherebyContainer').attr('data-code')
        } else {
            this.MonashCode = window.location.pathname.split('/')[1]
        }

        const identifier = new Date().getTime().toString()+'_'+Math.random().toString()

        const key = process.env.MIX_PUSHER_APP_KEY+':code:'+this.MonashCode
        window.Echo = new Echo({
            broadcaster: 'pusher',
            key,
            cluster: process.env.MIX_PUSHER_APP_CLUSTER,
            wsHost: window.location.hostname,
            wsPort: 6001,
            wssPort: 6001,
            enabledTransports: ['ws', 'wss'],
            forceTLS: true,
            auth: {
                headers: {
                    MonashCode: this.MonashCode
                },
            },
        })

        this.e = window.Echo.private(this.MonashCode)

        this.outside = !host
        if (!this.outside) {
            ready('inside')
        }
        this.e.on('pusher:subscription_succeeded', () => {
            // Other users may be partway through the video already
            // So ask them for state

            ready('subscribed')
            if (host) {
                this.e.listenForWhisper('isHostOnline', () => {
                    this.e.whisper('hostIsOnline')
                })
                this.e.whisper('hostIsOnline')
            } else {
                this.e.whisper('isHostOnline')
                this.e.listenForWhisper('hostIsOnline', () => {
                    ready('host')
                })
            }
        })
        when(['inside', 'subscribed', 'video_url']).then(() => {
            this.requestUpdate('load')

            this.e.listenForWhisper('change', data => {
                this.video.update(data)
            })
            this.video.onUpdate(data => {
                if (this.outside) {
                    return
                }
                delete data.event
                this.e.whisper('change', data)
            })

            this.e.listenForWhisper('reply', data => {
                if (this.updateRequestData !== undefined) {
                    if (this.updateRequestData.source !== 'load') {
                        const difference = (new Date().getTime()) - this.updateRequestData.time
                        if (difference > requestTimeout) {
                            return
                        }
                    }
                    clearTimeout(this.updateTimeout)

                    this.respondToUpdateRequestReply(data)
                }
            })
            this.e.listenForWhisper('request', () => {
                this.e.whisper('reply', this.video.getState())
            })

            this.e.listenForWhisper('countdownStart', () => {
                this.countdownInProgress = {time: new Date().getTime()}
                this.preventInteraction()
                this.e.whisper('askCountdownStarterForTime', {
                    identifier,
                    time: new Date().getTime(),
                    timeStarted: new Date().getTime(),
                    delays: []
                })
            })
            this.e.listenForWhisper('askCountdownStarterForTime', data => {
                if (this.countdownInProgress && this.countdownInProgress.startedByMe) {
                    this.e.whisper('countdownRespond', data)
                }
            })
            this.e.listenForWhisper('countdownRespond', data => {
                if (this.countdownInProgress && !this.countdownInProgress.startedByMe && data.identifier === identifier) {
                    const time = new Date().getTime()
                    data.delays.push((time - data.time) / 2)
                    data.time = time
                    if (data.delays.length < 3) {
                        this.e.whisper('askCountdownStarterForTime', data)
                    } else {
                        let total = 0
                        data.delays.forEach(delay => {
                            total += delay
                        })
                        const delay = total / data.delays.length
                        const timeStarted = data.timeStarted - delay

                        this.preventInteraction(countdownSyncDuration - (time - timeStarted))
                    }
                }
            })
        })


        let participantCount = 1
        let sawSomeoneLeave
        window.addEventListener('message', message => {
            if (message.data && message.data.type === 'participantupdate') {
                const newCount = message.data.payload.count
                const someoneLeft = newCount < participantCount
                participantCount = newCount

                if (this.outside) {
                    this.outside = false
                    $('.container').removeClass('knockRequired')
                    ready('inside')
                }

                if (someoneLeft) {
                    sawSomeoneLeave = new Date().getTime()
                    if (host) {
                        this.e.whisper('someoneLeft')

                        axios.post('/api/someoneLeft', {
                            code: hostCode
                        })
                    }
                }
            }
        })
        when(['inside', 'subscribed']).then(() => {
            let heardSomeoneLeave
            let noOneElseLeft
            let hostReportedSomeoneLeft
            const checkIfILeft = () => {
                if (noOneElseLeft !== undefined && hostReportedSomeoneLeft !== undefined && Math.abs(noOneElseLeft - hostReportedSomeoneLeft) < 60000) {
                    this.outside = true
                    unready('inside')

                    $('.container').empty()
                    queue(() => {
                        showModal('evicted')
                    })
                    this.e.unsubscribe(this.MonashCode)
                    this.e = undefined
                }
            }
            this.e.listenForWhisper('someoneLeft', () => {
                heardSomeoneLeave = new Date().getTime()
                setTimeout(() => {
                    if (sawSomeoneLeave === undefined || Math.abs(heardSomeoneLeave - sawSomeoneLeave) > 2000) {
                        noOneElseLeft = new Date().getTime()
                        checkIfILeft()
                    }
                }, 2000)
            })
            this.e.listen('SomeoneLeft', () => {
                hostReportedSomeoneLeft = new Date().getTime()
                checkIfILeft()
            })
        })

        this.video.room = this
    }
}
