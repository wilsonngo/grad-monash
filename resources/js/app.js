require('./bootstrap')
const $ = require('jquery')

import Ping from 'ping.js'

import { Video } from './video'
import { Room } from './room'
import { showModal, closeModal, queue } from './modals'
import { when, ready, unready, isReady } from './util/when'


let room

function resize() {
    const windowWidth = $('html').width()
    const $widescreen = $('.widescreen')
    if ($widescreen.length) {
        let width = windowWidth
        const widthPerHeight = $('.videoContainer').height() / 9 * 16
        if (widthPerHeight < width) {
            width = widthPerHeight
        }
        $widescreen.width(width)
    }


    // reposition the survey link
    const whereBy = $(".container .wherebyContainer");
    const whereByHeight = whereBy.height()
    const surveyCTAContainer = $('.survey-cta-wrapper');
    if (whereByHeight >= 350) {
        surveyCTAContainer.removeClass("top")
    } else {
        surveyCTAContainer.addClass("top")
    }
    // console.log(`height: ${whereByHeight}`)


    // resize whereBy position
    /*
    const whereBy = $(".container .wherebyContainer")
    const whereByiFrame = $(".container .wherebyContainer iframe")
    const frameParentHeight = whereBy.height()
    const frameHeight = whereByiFrame.height()
    const diff = frameParentHeight - frameHeight
    console.log(`Resize ${frameParentHeight} : ${frameHeight} = ${diff} `)
    if (diff > 150) {
        whereByiFrame.css("top","10%").css("bottom","")
    } else {
        whereByiFrame.css("top","").css("bottom","5px")
    }
    */

}
function updateInternetWarning() {
    const online = navigator.onLine
    if (online) {
        ready('online')
    } else {
        unready('online')
    }

    $('#internetWarning').toggleClass('hidden', online)
}

$(document).ready(() => {
    $('.modal').click(event => {
        const className = event.target.className
        if (className.startsWith('modal') || className.endsWith('close')) {
            let $modal = $(event.target)
            if (className.endsWith('close')) {
                $modal = $modal.parent().parent()
                if (!$modal.hasClass('modal')) {
                    return
                }
            }
            queue(() => {
                closeModal($modal.attr('id'), true)
            })
        }
    })

    $('.links .about_help').click(() => {
        queue(() => {
            showModal('about_help', true)
        })
        return false
    })
    $('.links .privacy_legal').click(() => {
        queue(() => {
            showModal('privacy_legal', true)
        })
        return false
    })
    $('a.survey_link').click(() => {
        $(".survey-cta-wrapper").removeClass("middle")
        queue(() => {
            showModal('survey', true)
        })
        return false
    })    

    const tooEarly = $('#early').length
    const tooLate = $('#late').attr('data-time') === undefined
    let askForPermissions = true
    if (!tooLate) {
        // Test for Safari, Firefox, Chrome or Chromium Edge
        const ua = navigator.userAgent.toLowerCase()
        const firefox = ua.indexOf('firefox') !== -1
        if (firefox) {
            $('body').addClass('firefox')
        } else {
            $('.firefox').remove()
        }

        let unsupported = (
            (ua.indexOf('safari') !== -1 && ua.indexOf('edge') === -1) ||
            firefox
        ) ? false : 'upgrade'
        if (!unsupported) {
            const iPhone = ua.indexOf('iphone os ') !== -1
            if (iPhone || ua.indexOf('cpu os ') !== -1) {
                const type = iPhone ? 'iphone os ' : 'cpu os '
                const iOS_version = parseInt(ua.split(type)[1].split('_')[0])
                if (iOS_version < 13) {
                    unsupported = 'upgrade'
                } else if (ua.indexOf('crios') !== -1) {
                    unsupported = 'ios_safari'
                } else {
                    // Asking for permissions in iOS Safari is not remembered
                    askForPermissions = false
                }
            }
        }
        if (!unsupported && $('.videoContainer').length) {
            const safari_desktop = ua.indexOf('safari') !== -1 && ua.indexOf('chrome') === -1
            if (safari_desktop) {
                // Asking for permissions in desktop Safari is not remembered
                askForPermissions = false
            }

            if (askForPermissions) {
                if (!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
                    unsupported = 'upgrade'
                }
            }
        }
        if (unsupported && window.location.search.indexOf('unsupported=false') === -1) {
            $('.background').addClass('unsupported')

            $('#unsupported .inner:not(#'+unsupported+')').remove()

            $('.container').empty()

            queue(() => {
                showModal('unsupported')
            })
            return
        }
    }
    $('.modal#unsupported').remove()

    const video = new Video()
    video.addAnimation(-1, 'B', 'LAYER 02_OVERLAY_ALWAYS ON_CLEAN.svg')
    video.addAnimation([-1, 1], 'C', 'LAYER 03_AUDIENCE_STANDING_SLIGHT MOTION_V2.svg')
    if (!$('.videoContainer').length) {
        // Home page, 404 or too late
        return
    }

    video.onVideoEnd((status, data) => {
        if (status) { //video ended event
            $(".survey-cta-wrapper").addClass("middle")
            if ($(".videoContainer").attr("data-video-ended-event") === "code_pause") {
                video.vi.pause();
                video.controls.updatePlaying(false);
            }
        }
    })

    video.onExpandVideoClick(() => {
        resize()
    });

    const p = new Ping({
        timeout: 1000
    })
    p.ping('https://vimeo.com', (err, _) => {
        if (video.china !== undefined) {
            return
        }
        video.china = err ? true : false
        if (window.location.search.indexOf('country=CHINA') !== -1) {
            video.china = true
        }
        if (video.china) {
            ready('country')
        } else {
            $.getScript('https://player.vimeo.com/api/player.js').then(() => {
                ready('country')
            })
        }
    })

    $(window).bind('online offline', updateInternetWarning)
    updateInternetWarning()

    let name = $('.wherebyContainer').attr('data-name')
    $('.modal#name form').submit(function () {
        const $text = $(this).find('input[type="text"]')

        const val = $text.val()
        if (val.length > 20) {
            if (!$('.modal#name .error').length) {
                const $error = $('<div />')
                $error.addClass('error')
                $error.text('Name must be 20 characters or less')
                $error.insertAfter($text)
            }
        } else {
            name = val
            queue(() => {
                closeModal('name')
            })
        }
        return false
    })

    when('active').then(() => {
        const host = $('.wherebyContainer').attr('data-host') === 'true'
        const introModal = $('#intro').length

        when(['inside', 'video_url']).then(() => {
            video.setup()

            window.addEventListener('resize', resize)
            resize()

            $('a.survey-cta').click(() => {
                $(".survey-cta-wrapper").removeClass("middle")
                queue(() => {
                    showModal('survey', true)
                })
                return false
            })

            setTimeout(function(){
                // show the survey cta
                $('.survey-cta-wrapper').hide().removeClass('hidden').fadeIn("slow")
            }, 5000)
            

        })

        const afterFinalName = () => {
            when('online').then(() => {
                // Add Whereby
                $('footer').addClass('cricos')

                const $iframe = $('<iframe />').addClass('whereby')
                const roomUrl = $('.wherebyContainer').attr('data-roomUrl')
                $iframe.attr('src', roomUrl+'&displayName='+encodeURIComponent(name))
                $iframe.attr('allow', 'camera; microphone; speaker')
                $('.wherebyContainer').append($iframe)

                let loaded
                $('.wherebyContainer iframe').on('load', e => {
                    if (loaded) {
                        // Loaded a second time
                        // Perhaps the user reloaded the iframe, but more likely is that they were rejected and have clicked 'Leave Room'
                        queue(() => {
                            showModal('left')
                        })
                        $('.wherebyContainer').remove()
                    } else {
                        loaded = true
                    }
                })

                // Update URL
                const parts = window.location.pathname.split('/')
                if (parts[2] !== name) {
                    let path = window.location.protocol + '//' + window.location.host
                    if (!parts[1]) {
                        parts[1] = 'code'
                    }
                    parts[2] = name
                    path += parts.join('/') + window.location.search

                    window.history.pushState({path}, '', path)
                }
            })
        }
        const afterName = () => {
            if (name) {
                room.setName(name)
                afterFinalName()
            } else {
                room.setName().then(finalName => {
                    name = finalName
                    afterFinalName()
                })
            }
            if (!host) {
                when('subscribed').then(() => {
                    setTimeout(() => {
                        if (!isReady('host')) {
                            queue(() => {
                                showModal('waitingForHost')
                            })
                        }
                    }, 5000)
                })
            }
        }
        const afterPermissions = () => {
            if (name) {
                afterName()
            } else {
                queue(() => {
                    showModal('name').then(afterName)
                })
            }
        }
        const afterIntro = () => {
            queue(() => {
               showModal('permissions').then(afterPermissions)
               if (askForPermissions) {
                   navigator.mediaDevices.getUserMedia({
                       video: true,
                       audio: true
                   }).then(() => {
                       ready('permissions')
                   })
                   if (!introModal) {
                       when('permissions').then(() => {
                           queue(() => {
                               closeModal('permissions')
                           })
                       })
                   }
                }
            })
        }

        $('body').addClass('duringVideo')

        let celebration = $('.videoContainer').attr('data-time')

        video.addAnimation(0, 'A', 'LAYER 01_BG_CEREMONY BEGIN_PART 2.svg')
        video.addAnimation([0, 60], 'C', 'LAYER 03_AUDIENCE_SITDOWN.svg')
        video.addAnimation([0, 60], 'E', 'LAYER 05_SPOTLIGHT_FADE UP.svg')

        video.addAnimation(60, 'E', 'LAYER 05_SPOTLIGHT_ON.svg')
        if (celebration !== undefined) {
            celebration = parseInt(celebration)
            video.addAnimation([60, celebration-60], 'C', 'LAYER 03_AUDIENCE_STATIC.svg')

            video.addAnimation([celebration, 7], 'C', 'LAYER 03_AUDIENCE_CELEBRATE_TRIGGERED.svg')
            video.addAnimation([celebration, 5.13], 'D', 'LAYER 04_CONFETTI_TRIGGERED WITH CELEBRATE.svg')

            video.addAnimation(celebration+7, 'C', 'LAYER 03_AUDIENCE_STATIC.svg')
        } else {
            video.addAnimation(60, 'C', 'LAYER 03_AUDIENCE_STATIC.svg')
        }

        if ($('.wherebyContainer').length) {
            room = new Room(host, video)
            if (introModal) {
                queue(() => {
                    showModal('intro').then(afterIntro)
                })
            } else {
                afterIntro()
            }
        } else {
            ready('inside')
            video.fetchCountry()
        }
    })

    let checkTime
    const timeFromSeconds = seconds => new Date().getTime() + parseInt(seconds) * 1000
    const lateTime = $('#late').attr('data-time')
    const events = [
        {
            time: timeFromSeconds(lateTime),
            callback: () => {
                $('.container').empty()
                $('body').removeClass('duringVideo')
                queue(() => {
                    showModal('late')
                })

                clearInterval(checkTime)
            }
        }, {
            time: timeFromSeconds(lateTime) - 30*60*1000,
            callback: () => {
                const $warning = $('#halfHourWarning')

                let timeLeft = ''
                const secondsLeft = parseInt(lateTime)
                if (secondsLeft > 59) {
                    const minutesLeft = Math.floor(lateTime / 60)
                    timeLeft = minutesLeft.toString()+' minute'
                    if (minutesLeft != 1) {
                        timeLeft += 's'
                    }
                } else {
                    timeLeft = 'under a minute'
                }
                $warning.find('.timeLeft').text(timeLeft)
                $warning.removeClass('hidden')
            }
        }
    ]

    if (tooEarly) {
        queue(() => {
            showModal('early')
        })

        events.push({
            time: timeFromSeconds($('#early').attr('data-time')),
            callback: () => {
                queue(() => {
                    closeModal('early')
                })

                if (!video.url) {
                    video.fetchCountry()
                }
                ready('active')
            }
        })
    } else {
        ready('active')
    }

    checkTime = setInterval(() => {
        for (let i=0; i<events.length; i++) {
            const event = events[i]
            if (event.time < new Date().getTime()) {
                event.callback()

                events.splice(i, 1)
                break
            }
        }
    }, 1000)
})
