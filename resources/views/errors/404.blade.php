@include('partials/header')
<div class="container" style="display: flex;flex-direction: column;">
    @if (!empty(Config::get('database.connections.mysql.password')) && in_array(App::environment(), ['local', 'development']))
        <div style="max-width: 500px;margin: 1em auto 0 auto;padding:1em;background-color:#fff;color:#000;text-align:center;">
            This is a 404 page.<br /><br />
            For testing purposes, see these rooms
            <ul style="padding-left: 0;list-style:none;max-height:300px;overflow:auto;margin:0;">
                @foreach (App\Models\User::all()->sort(function ($a, $b) {
                    $aName = $a->name;
                    $bName = $b->name;
                    $aNumber = strpos($aName, ' ') !== false;
                    $bNumber = strpos($bName, ' ') !== false;
                    if ($aNumber === $bNumber) {
                        if ($aNumber) {
                            $aCeremony = strpos($aName, 'Ceremony') === 0;
                            $bCeremony = strpos($bName, 'Ceremony') === 0;
                            if ($aCeremony === $bCeremony) {
                                $index = $aCeremony ? 8 : 5;
                                return intval(substr($aName, $index)) <=> intval(substr($bName, $index));
                            }
                        }
                        return strcmp($aName, $bName);
                    } else {
                        return $aNumber ? 1 : -1;
                    }
                }) as $user)
                    <li>
                        {{$user->name}}@if (!empty($user->celebration_time)) ({{ floor($user->celebration_time / 60) }}m {{ floor($user->celebration_time % 60) }}s)@endif:
                        <a href="/{{$user->guest_code}}" style="color: #000;">Guest</a>
                        @if (!empty($user->host_code)) or <a href="/{{$user->host_code}}" style="color: #000;">Host</a>@endif
                    </li>
                @endforeach
            </ul>
        </div>
    @else
        @include('partials/modals/notFound')
    @endif
</div>
@include('partials/footer')
