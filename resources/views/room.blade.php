@include('partials/header')
    @if (!empty($event))
        <div class="container @if (!$videoOnly && !$host) knockRequired @endif">
            @if (!$event->hasEnded())
                <div data-video-ended-event="{{ $event->videoEndActionEvent() }}"  class="videoContainer"@if (!empty($celebration_time)) data-time="{{ $celebration_time }}"@endif>
                    @if ($event->surveyURL() !== "")
                    <div class="survey-cta-wrapper hidden"><a class="survey-cta" href="javascript:void(0)" rel="noopener noreferrer">Tell us about your experience.</a></div>
                    @endif
                </div>
                @if (!$videoOnly)
                    <div class="wherebyContainer" data-roomUrl="{{ $roomUrl }}"
                        @if ($name) data-name="{{ $name }}" @endif
                        @if ($host) data-host="true" data-code="{{ $guest_code }}" @endif
                    ></div>
                @endif
            @endif
        </div>
    @endif
    @if (empty($event) || !empty($event->ends_at))
        @include('partials/modals/late')
    @endif

    @if (!empty($event) && !$event->hasEnded())
        @if (!empty($starts_at))
            @include('partials/modals/early')
        @endif

        @include('partials/modals/unsupported')

        @if (!$videoOnly)
            <div id="halfHourWarning" class="hidden">This viewing window will close in <span class="timeLeft"></span></div>
            @if ($introModal)
                @include('partials/modals/intro')
            @endif
            @if (!$name)
                @include('partials/modals/name')
            @endif
            @if (!$host)
                @include('partials/modals/waitingForHost')
            @endif

            @include('partials/modals/left')
            @include('partials/modals/interaction')
            @include('partials/modals/permissions')
            @include('partials/modals/evicted')
        @endif

        <div id="internetWarning" class="hidden">
            Internet connection issue detected<br />
            We've detected issues with your internet connection that could affect your experience. Please check your connection and try again.<br />
            If your connection is stable and you are still seeing this message, please <a href="https://www.monash.edu/virtual-graduations" target="_blank" rel="noopener noreferrer">contact Monash support</a>
        </div>
    @endif
@include('partials/footer')
