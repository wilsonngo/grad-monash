<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Monash {{ date('Y').' '.config('app.name') }}</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}" />
        <script>
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-KFS435R');
        </script>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    </head>
    <body @if (!empty($modal)) class="modalVisible"@endif @if (!empty($videoOnly)) data-videoOnly="true" @endif>
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KFS435R" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <div class="background">
            <object type="image/svg+xml" data-layer="A" data-name="LAYER 01_BG_INTRO_CLEAN_V2.svg" data="/images/animations/LAYER 01_BG_INTRO_CLEAN_V2.svg"></object>
            <object type="image/svg+xml" data-layer="B" data-name="LAYER 02_OVERLAY_ALWAYS ON_CLEAN.svg" data="/images/animations/LAYER 02_OVERLAY_ALWAYS ON_CLEAN.svg"></object>
        </div>
        <div class="header">
            <a href="https://www.monash.edu/" target="_blank" rel="noopener noreferrer" class="logo">
                <img src="{{ asset('images/logo-monash.png') }}" alt="Monash University" />
            </a>
            <a href="https://www.monash.edu/virtual-graduations" target="_blank" rel="noopener noreferrer" class="title">
                <div class="year">{{ date('Y') }}</div>
                <img src="{{ asset('images/logo-grad.png') }}" alt="Graduation Celebration" />
            </a>
        </div>
        @include('partials/modals/unsupported')
