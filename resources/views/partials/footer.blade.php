        @include('partials/modals/about_help')
        @include('partials/modals/privacy_legal')
        @include('partials/modals/survey')
        <footer @if (!empty($event) && $name)class="cricos"@endif>
            <div class="links">
                <a href="javascript:void(0);" class="about_help">About | Help</a><a href="javascript:void(0);" class="privacy_legal">Privacy | Legal</a>
            </div>
            <div class="text">
                <div class="cricos"><span>CRICOS Provider:</span><span> Monash University 00008C/01857J</span></div>
                <div class="respects">We acknowledge and pay respects to the Elders and Traditional Owners of the land on which our four Australian campuses stand. <strong><a href="https://www.monash.edu/indigenous-australians" target="_blank" rel="noopener noreferrer">Information for Indigenous Australians</a></strong></div>
            </div>
        </footer>
    </body>
</html>
