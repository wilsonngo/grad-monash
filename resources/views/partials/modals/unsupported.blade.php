<div class="modal" id="unsupported">
    <div class="inner" id="upgrade">
        <h2>Please upgrade your browser</h2>
        It looks like you are using a web browser version that does not support this experience. Make sure you are using the most recent version of Google Chrome, Firefox, Safari or Microsoft Edge to join the experience
        <ul>
            <li>
                <img src="{{ asset('images/browsers/chrome.svg') }}" alt="Chrome" />
            </li><li>
                <img src="{{ asset('images/browsers/firefox.svg') }}" alt="Firefox" />
            </li><li>
                <img src="{{ asset('images/browsers/safari.svg') }}" alt="Safari" />
            </li><li>
                <img src="{{ asset('images/browsers/edge.svg') }}" alt="Edge" />
            </li>
        </ul>
        If you are using the latest version of a browser indicated above, please <a href="https://www.monash.edu/virtual-graduations" target="_blank" rel="noopener noreferrer">get in touch with our support team</a>
    </div>
    <div class="inner" id="ios_safari">
        It looks like you're using Chrome on iOS. Please change to Safari.
        <ul>
            <li>
                <img src="{{ asset('images/browsers/safari.svg') }}" alt="Safari" />
            </li>
        </ul>
        If this is not the case, please <a href="https://www.monash.edu/virtual-graduations" target="_blank" rel="noopener noreferrer">get in touch with our support team</a>
    </div>
</div>
