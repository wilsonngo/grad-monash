<div class="modal visible" id="welcome">
    <div class="inner">
        <h2>Welcome to a Monash University Graduation Celebration</h2>
        To access your personal graduation party link, please refer to the email sent to all graduates. If you cannot find the email, you can email us at <a href="mailto:virtualgraduationsupport@monash.edu" target="_blank" rel="noopener noreferrer">virtualgraduationsupport@monash.edu</a> to get in touch with our support team.<br /><br />
        For more information about Monash Graduation {{ date('Y') }}, please head to <a href="https://www.monash.edu/virtual-graduations" target="_blank" rel="noopener noreferrer">https://www.monash.edu/virtual-graduations</a>.
    </div>
</div>
