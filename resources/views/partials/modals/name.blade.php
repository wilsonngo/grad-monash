<div class="modal" id="name">
    <form class="inner">
        <a href="javascript:void(0);" class="close"></a>
        <h2>Name</h2>
        Please set your name using the field below and then join the celebration.
        <input type="text" name="monashGradName" placeholder="Enter your name" />
        <input type="submit" value="Join" />
    </form>
</div>
