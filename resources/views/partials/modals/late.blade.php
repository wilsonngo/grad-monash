<div class="modal @if (!empty($modal) && $modal == 'late') visible @endif" id="late" @if (!empty($event) && !$event->hasEnded()) data-time="{{ $event->ends_at->diffInSeconds(now()) }}" @endif>
    <div class="inner">
        This Graduation Celebration has finished.<br />
        You can still <a href="https://www.monash.edu/virtual-graduations/virtual-graduation-celebration-videos" target="_blank" rel="noopener noreferrer">view the video anytime here</a>
    </div>
</div>
