<div class="modal" id="evicted">
    <div class="inner">
        The host has removed you from this Monash Graduation Celebration.<br />
        If you believe this is a mistake, please get in touch with your host, or reload the page and knock for the host to let you back in.
    </div>
</div>
