<div class="modal" id="permissions">
    <div class="inner">
        <a href="javascript:void(0);" class="close"></a>
        <h2>Permission</h2>
        This experience requires access to your camera and microphone. Please click "allow" when prompted<span class="firefox">, and tick "Remember this decision"</span>. For video conferencing, the experience uses a service called Whereby. Please allow Whereby permission as well if prompted.
        <button class="close">Next</button>
    </div>
</div>
