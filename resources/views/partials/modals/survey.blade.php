@if (!empty($event))
    <div class="modal" id="survey">
        <div class="inner inner-survey">
            <a href="javascript:void(0);" class="close"></a>
            <h2>Tell us about your experience</h2>
            <iframe 
                class="survey"
                src="{{@$event->surveyURL()}}" 
                frameborder="0"></iframe>
            <p>
                <a rel="noopener noreferrer" target="_blank"
                href="{{@$event->surveyURL()}}">Open feedback in new tab.</a>
            </p>
        </div>
    </div>
@endif