<div class="modal visible" id="notFound">
    <div class="inner">
        <h2>Oops</h2>
        That page could not be found.<br />
        Please check your e-mail for the link.
    </div>
</div>
