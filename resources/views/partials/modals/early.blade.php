<div class="modal" id="early" data-time="{{ $starts_at->diffInSeconds(now()) }}">
    <div class="inner">
        <h2>Welcome to a Monash University<br />Graduation Celebration</h2>
        It looks like you're early.<br />
        @if ($videoOnly)
            The ceremony video will be available at 11am on <strong>{{ $starts_at->format('F jS Y') }}</strong>.<br />
            Please check back then.
        @else
            The viewing window for this celebration is between 11am and {{ $reported_ends_at === '12am' ? 'midnight' : $reported_ends_at }} on<br /><strong>{{ $starts_at->format('j F Y') }}</strong>.<br />
            Please return to enjoy the graduation.
        @endif
    </div>
</div>
