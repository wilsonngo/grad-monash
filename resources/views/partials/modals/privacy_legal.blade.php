<div class="modal" id="privacy_legal">
    <div class="inner">
        <a href="javascript:void(0);" class="close"></a>
        <h2>Privacy | Legal</h2>
        We are using essential cookies to ensure this website operates correctly and to capture anonymous analytics for the purposes of internal reporting only.<br /><br />
        Copyright &copy; {{ date('Y') }} Monash University. ABN 12 377 614 012<br /><br />
        <a href="https://www.monash.edu/accessibility" target="_blank" rel="noopener noreferrer">Accessibility</a> - <a href="https://www.monash.edu/disclaimer-copyright" target="_blank" rel="noopener noreferrer">Disclaimer and copyright</a> / <a href="https://www.monash.edu/terms-and-conditions" target="_blank" rel="noopener noreferrer">Website terms and conditions</a> / <a href="https://www.monash.edu/privacy-monash" target="_blank" rel="noopener noreferrer">Data Protection and Privacy Procedure</a><br /><br />
        Monash University CRICOS Provider Number: 00008C
    </div>
</div>
