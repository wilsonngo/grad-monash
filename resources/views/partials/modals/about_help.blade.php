<div class="modal" id="about_help">
    <div class="inner">
        <a href="javascript:void(0);" class="close"></a>
        <h2>About | Help</h2>
        This is a shared experience for Monash students and their friends and family. Everyone who has joined this celebration will watch the video in sync once it begins playing. Everyone will have the ability to pause and play the video content.<br /><br />
The site uses a video conferencing service called Whereby. When prompted, you need to allow Whereby to access your camera and microphone.<br /><br />
Monash graduates are 'hosts' of their grad party. As the host, you'll receive a message each time a guest enters the video conference to provide them access. You'll need to click 'let in' before you can see your guest.<br /><br />
As a guest, you'll be required to 'knock' and wait for your host to let you in.<br /><br />
We’re confident the platform works in all regions of the world, however some people may experience localised accessibility issues. If you are unable to get the platform to work, you’ll be able to access your celebration video directly from this website: <a href="https://www.monash.edu/virtual-graduations/virtual-graduation-celebration-videos" target="_blank" rel="noopener noreferrer">https://www.monash.edu/virtual-graduations/virtual-graduation-celebration-videos</a>. If you access the celebration video this way, you can still still share the experience with your guests by simultaneously accessing the video and setting up your own video conference or chat room (i.e Zoom or WeChat).<br /><br />
Additional FAQs are available here: <a href="https://www.monash.edu/virtual-graduations#FAQs" target="_blank" rel="noopener noreferrer">https://www.monash.edu/virtual-graduations#FAQs</a><br /><br />
Alternatively you can email us at <a href="mailto:virtualgraduationsupport@monash.edu">virtualgraduationsupport@monash.edu</a> for additional technical support.
    </div>
</div>
