<div class="modal" id="intro">
    <div class="inner">
        <a href="javascript:void(0);" class="close"></a>
        <h2>Welcome to a Monash University Graduation Celebration</h2>
        This is a shared experience for Monash students and their friends and family. Everyone who joins this celebration room will watch the ceremony in sync once it begins playing. Everyone will be able to control the ceremony content, similar to everyone having a remote to watch something on TV together.
        <button class="close">Next</button>
    </div>
</div>
