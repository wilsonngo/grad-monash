<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Video;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $video = new Video();
        $video->starts_at = now();
        $video->vimeo_url = 'https://vimeo.com/472045151/e75955cd39';
        $video->tencent_url = 'https://1257870645.vod2.myqcloud.com/da6afa0fvodcq1257870645/d33eacfe5285890809589714674/f0.mp4';
        $video->room_hours_duration = 13;
        $video->save();

        $user = new User();
        $user->name = 'John';
        $user->video_id = $video->id;
        $user->celebration_time = 20*60+2;
        $user->save();
    }
}
